User-agent: *
Allow: /
Allow: /archives/
Allow: /categories/
Allow: /tags/ 
Allow: /2016/
Allow: /2017/
Disallow: /vendors/
Disallow: /js/
Disallow: /css/
Disallow: /fonts/
Disallow: /vendors/
Disallow: /fancybox/


Sitemap: https://www.githubxxcc.github.io/sitemap.xml
Sitemap: https://www.githubxxcc.github.io/baidusitemap.xml
